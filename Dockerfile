# Pull base image.
FROM registry.gitlab.com/lehoang03triet/docker-android-nodejs:8-9-1

# ————————————————————————————————————————
# Install Basic React-Native packages
# ————————————————————————————————————————
RUN npm install react-native-cli -g
RUN npm install yarn -g

ENV LANG en_US.UTF-8
